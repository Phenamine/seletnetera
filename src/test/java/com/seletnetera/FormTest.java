package com.seletnetera;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class FormTest {
    public  static FormPage formPage;
    public  static WebDriver driver;
    @BeforeClass
    public static void setup() throws IOException {
        WebDriverManager.chromedriver().browserVersion(ReadConfigs.getProperty("chromeVersion")).setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=cs-CS");
        driver = new ChromeDriver(options);
        formPage = new FormPage(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(ReadConfigs.getProperty("formUrl"));

    }

    @Test
    public void formTestPos(){
        formPage.clickQuestion1AnswerByText("Možnost 3");
        formPage.inputQuestion2Answer("This is my answer");

        formPage.clickQuestion3AnswerByText("Možnost 2");
        formPage.clickQuestion3AnswerByText("Možnost 1");
        formPage.clickQuestion3AnswerByText("Možnost 3");

        formPage.inputDate("09.11.2020");
        formPage.inputHour("22");
        formPage.inputMinute("58");

        formPage.clickQuestion4AnswerByText(FormPage.Question4answerNumber.SECOND);

        formPage.clickSendButton();

        Assert.assertTrue(formPage.getConfirmationMessage().equals("Vaše odpověď byla zaznamenána."));
    }

    @AfterClass
    public static void tearDown() {
        driver.quit(); }
}
