package com.seletnetera;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class FormPage {
    private static final Logger LOGGER = Logger.getLogger(FormPage.class.getName());
    FileHandler fileHandler= new FileHandler("logs.log");

    public WebDriver driver;
    public FormPage(WebDriver driver) throws IOException {
        LOGGER.addHandler(fileHandler);
        SimpleFormatter formatter = new SimpleFormatter();
        fileHandler.setFormatter(formatter);

        PageFactory.initElements(driver, this);
        this.driver = driver; }


    @FindBy(xpath = "//div[contains(@data-params,'Otázka 1')]")
    private WebElement question1Element;

    @FindBy(xpath = "//div[contains(@data-params,'Otázka 2')]")
    private WebElement question2Element;

    @FindBy(xpath = "//div[contains(@data-params,'Otázka 3')]")
    private WebElement question3Element;

    @FindBy(xpath = "//div[contains(@data-params,'Otázka 4')]")
    private WebElement question4Element;

    @FindBy(xpath = "//input[@type='date']")
    private WebElement date;

    @FindBy(xpath = "//input[@type='text' and @aria-label='Hodina']")
    private WebElement hour;

    @FindBy(xpath = "//input[@type='text' and @aria-label='Minuta']")
    private WebElement minute;

    @FindBy(xpath="//form[@id='mG61Hd']/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[2]/div[3]/span")
    private WebElement question4answer1;

    @FindBy(xpath="//form[@id='mG61Hd']/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[2]/div[4]/span")
    private WebElement question4answer2;

    @FindBy(xpath="//form[@id='mG61Hd']/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[2]/div[5]/span")
    private WebElement question4answer3;

    @FindBy(xpath = "//span[text()='Odeslat']/parent::span")
    private WebElement sendButton;

    @FindBy(xpath = "//div[contains(@class,'freebirdFormviewerViewResponseConfirmationMessage')]")
    private WebElement confirmMessage;



    enum Question4answerNumber{
        FIRST,
        SECOND,
        THIRD
    }

    public void clickQuestion1AnswerByText(String myText){
        WebElement elementByText = this.question1Element.findElement(By.xpath("//span[contains(text(),'" + myText + "')]"));
        elementByText.click();
        LOGGER.info("Q1 Element was clicked: " + myText);
    }

    public void inputQuestion2Answer(String myText){
        this.question2Element.findElement(By.tagName("input")).sendKeys(myText);
        LOGGER.info("Q2 answer was typed: " + myText);
    }

    public void clickQuestion3AnswerByText(String myText){
        WebElement element = this.question3Element.findElement(By.xpath("//div[@data-answer-value='" + myText + "']"));
        element.click();
        LOGGER.info("Q3 answer was clicked: " + myText);
    }

    public void clickDropDownQuestion4(){
        this.question4Element.findElement(By.xpath(".//div[contains(@class,'quantumWizMenuPaperselectDropDown')]")).click();
    }

    public void clickQuestion4AnswerByText(Question4answerNumber answerNumber){
        clickDropDownQuestion4();
        switch (answerNumber){
            case FIRST:
                question4answer1.click();
                LOGGER.info("Q4 1st answer was clicked");
                break;
            case SECOND:
                question4answer2.click();
                LOGGER.info("Q4 2nd answer was clicked");
                break;
            case THIRD:
                question4answer3.click();
                LOGGER.info("Q4 3rd answer was clicked");
                break;
        }

    }

    public void inputDate(String date){
        this.date.sendKeys(date);
        LOGGER.info("Date input: " + date);
    }

    public void inputHour(String hour){
        this.hour.sendKeys(hour);
        LOGGER.info("Hour input: " + hour);
    }

    public void inputMinute(String minute){
        this.minute.sendKeys(minute);
        LOGGER.info("Minutes input: " + minute);
    }

    public void clickSendButton(){
        WebElement el = this.sendButton;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        el.click();
        LOGGER.info("Submit button was clicked");
    }

    public String getConfirmationMessage(){
        return confirmMessage.getText();
    }


}
